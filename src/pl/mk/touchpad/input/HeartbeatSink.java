package pl.mk.touchpad.input;

public class HeartbeatSink implements InputConsumer {

  @Override
  public boolean serviceInput(ParsedInput parsedInput) throws ShutdownException{
    if(parsedInput.inputType == InputType.SHUTDOWN) {
      throw new ShutdownException("Shutdown command received");
    }
    return true;
  }

}
