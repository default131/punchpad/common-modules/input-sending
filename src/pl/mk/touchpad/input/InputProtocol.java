package pl.mk.touchpad.input;

import java.util.Arrays;

import javax.crypto.spec.IvParameterSpec;

import pl.mk.touchpad.crypto.encoding.Coding;
import pl.mk.touchpad.crypto.encryption.DecrypterService;
import pl.mk.touchpad.crypto.encryption.EncrypterService;
import pl.mk.touchpad.crypto.encryption.Pair;
import pl.mk.touchpad.crypto.encryption.Processor;

public class InputProtocol {
  //private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(InputProtocol.class.getName());
  
  public static final int LEN_IV = Processor.IV_SIZE_BYTES;
  public static final int LEN_HASH = 32;
  public static final int LEN_DATA = 7;
  public static final int LEN_FULL_PAYLOAD = LEN_IV + LEN_HASH + LEN_DATA;

  public static final int TIMEOUT_MS = 40000;
  public static final int HEARTBEAT_INTERVAL_MS = (int) (TIMEOUT_MS/2.5);
  
  public static IvParameterSpec preparePayload(EncrypterService encrypter, byte[] payload, byte[] hash, ParsedInput input) {
    byte[] data = new byte[LEN_DATA];
    data[0] = input.inputType.getCode();

    byte[] coded_x = Coding.encodeInt16(input.x);
    data[1] = coded_x[0];
    data[2] = coded_x[1];

    byte[] coded_y = Coding.encodeInt16(input.y);
    data[3] = coded_y[0];
    data[4] = coded_y[1];
    
    byte[] coded_z = Coding.encodeInt16(input.z);
    data[5] = coded_z[0];
    data[6] = coded_z[1];
    
    return preparePayload(encrypter, payload, hash, data);
  }
  
  private static IvParameterSpec preparePayload(EncrypterService encrypter, byte[] payload, byte[] hash, byte[] data) {
    byte[] plainTextCombined = new byte[LEN_HASH + LEN_DATA];

    System.arraycopy(
        hash, 
        0, 
        plainTextCombined, 
        0, 
        LEN_HASH
    );
    
    System.arraycopy(
        data, 
        0, 
        plainTextCombined, 
        LEN_HASH, 
        LEN_DATA
    );
    
    Pair<byte[], IvParameterSpec> cipherTextCombined_iv = encrypter.encrypt(plainTextCombined);
    
    //put iv
    System.arraycopy(
        cipherTextCombined_iv.b.getIV(),
        0,
        payload,
        0,
        LEN_IV
    );

    //put encrypted hash
    System.arraycopy(
        cipherTextCombined_iv.a,
        0,
        payload,
        LEN_IV,
        LEN_HASH
    );
    
    //put encrypted data
    System.arraycopy(
        cipherTextCombined_iv.a,
        LEN_HASH,
        payload,
        LEN_IV + LEN_HASH,
        LEN_DATA
    );
    
    return cipherTextCombined_iv.b;
  }
  
  /**
   * 
   * @param decrypter
   * @param payload
   * @return hash, parsed input
   */
  public static Pair<byte[], ParsedInput> decryptPacket(DecrypterService decrypter, byte[] payload) {
    byte[] decryptedPayload = decryptPacketRaw(decrypter, payload);

    return new Pair<byte[], ParsedInput>(
        Arrays.copyOfRange(decryptedPayload, 0, InputProtocol.LEN_HASH),
        new ParsedInput(
            getInputType(decryptedPayload),
            getX(decryptedPayload),
            getY(decryptedPayload),
            getZ(decryptedPayload)
        )
    );
  }
  
  private static byte[] decryptPacketRaw(DecrypterService decrypter, byte[] payload) {
    byte[] iv = Arrays.copyOfRange(
        payload,
        0,
        LEN_IV
    );
    byte[] hash_data = decrypter.decrypt(
        Arrays.copyOfRange(
            payload,
            LEN_IV, 
            LEN_IV + LEN_HASH + LEN_DATA
        ), 
        iv
    );
    
    return hash_data;
  }
  
  private static InputType getInputType(byte[] payload) {
    return InputType.valueOf(payload[InputProtocol.LEN_HASH + 0]);
  }
  
  private static int getX(byte[] payload) {
    return Coding.decodeInt16(
        payload[LEN_HASH + 1],
        payload[LEN_HASH + 2]
    );
  }
  
  private static int getY(byte[] payload) {
    return Coding.decodeInt16(
        payload[LEN_HASH + 3],
        payload[LEN_HASH + 4]
    );
  }
  
  private static int getZ(byte[] payload) {
    return Coding.decodeInt16(
        payload[LEN_HASH + 5],
        payload[LEN_HASH + 6]
    );
  }
}
