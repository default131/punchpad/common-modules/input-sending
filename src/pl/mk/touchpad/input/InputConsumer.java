package pl.mk.touchpad.input;

public interface InputConsumer {
  public boolean serviceInput(ParsedInput parsedInput) throws ShutdownException;
}
