package pl.mk.touchpad.input;

public class PacketHandlingException extends Exception {
  private static final long serialVersionUID = 1L;

  public PacketHandlingException() {
  }
  
  public PacketHandlingException(String message) {
    super(message);
  }
}
