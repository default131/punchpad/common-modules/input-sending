package pl.mk.touchpad.input;

import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pl.mk.touchpad.crypto.encryption.DecrypterService;
import pl.mk.touchpad.crypto.encryption.EncrypterService;

public class InputSender extends InputPeer implements InputConsumer {

  protected final ExecutorService executorService = Executors.newCachedThreadPool();

  public InputSender(
      EncrypterService encrypter,
      DecrypterService decrypter,
      byte[] hash,
      DatagramSocket socket,
      InputConsumer inputConsumer,
      Runnable timeoutCallback) 
      throws SocketException {
    super(
        encrypter,
        decrypter,
        hash,
        socket,
        inputConsumer,
        timeoutCallback
    );
  }

  @Override
  public boolean serviceInput(ParsedInput parsedInput) {
    try {
      return executorService.submit(() -> sendPacket(parsedInput)).get();
    } catch (ExecutionException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return false;
  }
}

