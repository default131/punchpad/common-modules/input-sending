package pl.mk.touchpad.input;

public class ShutdownException extends Exception {
  private static final long serialVersionUID = 1L;

  public ShutdownException(String message) {
    super(message);
  }
  
  public ShutdownException() {
    
  }
}
