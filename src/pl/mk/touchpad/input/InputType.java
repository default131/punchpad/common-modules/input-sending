package pl.mk.touchpad.input;

public enum InputType {
  MOVEMENT((byte)0),
  LEFT_PRESS((byte)1),
  RIGHT_PRESS((byte)2),
  RELEASE_MOUSE_LEFT((byte)4),
  RELEASE_MOUSE_RIGHT((byte)8),
  LEFT_CLICK((byte)10),
  SCROLL((byte)5),
  HEARTBEAT((byte)6),
  SHUTDOWN((byte)7);
  
  private byte code;
  
  InputType(byte intType) {
    this.code = intType;
  }
  
  public byte getCode() {
    return this.code;
  }
  
  static InputType valueOf(byte code) throws NumberFormatException{
    InputType[] types = InputType.values();
    for(InputType type : types)
      if(type.code == code)
        return type;
    
    throw new IllegalArgumentException("Passed illegal code " + code);
  }
}
