package pl.mk.touchpad.input;

public class ParsedInput {
  public final InputType inputType;
  public final int x;
  public final int y;
  public final int z;

  public ParsedInput(InputType inputType) {
    this.inputType = inputType;
    this.x = 0;
    this.y = 0;
    this.z = 0;
  }
  
  public ParsedInput(InputType inputType, int x, int y, int z) {
    this.inputType = inputType;
    this.x = x;
    this.y = y;
    this.z = z;
  }
  
  @Override
  public String toString() {
    return inputType.toString() + ", " + x + ":" + y + ":" + z;
  }
}
