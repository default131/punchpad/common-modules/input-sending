package pl.mk.touchpad.input;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import pl.mk.touchpad.crypto.encryption.DecrypterService;
import pl.mk.touchpad.crypto.encryption.EncrypterService;
import pl.mk.touchpad.crypto.encryption.Pair;

public class InputPeer extends Thread {
  private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(InputPeer.class.getName());
  
  protected static final int MAX_PACKET_BACKLOG = 2;
  protected static final int TOTAL_SOCKET_BACKLOG_BUFFER = 
      InputProtocol.LEN_FULL_PAYLOAD * MAX_PACKET_BACKLOG;
  
  protected final DatagramSocket datagramSocket;
  
  //payload and packet for sending data and heartbeats
  protected final byte[] payload_send = new byte[InputProtocol.LEN_FULL_PAYLOAD];
  protected final DatagramPacket packet_send = new DatagramPacket(payload_send, payload_send.length);

  //payload and packet for receiving data and heartbeats
  protected final byte[] payload_receive = new byte[InputProtocol.LEN_FULL_PAYLOAD];
  protected final DatagramPacket packet_receive = new DatagramPacket(payload_receive, payload_receive.length);
  
  protected final DecrypterService decrypter;
  protected final EncrypterService encrypter;
  protected final byte[] hash;
  protected final InputConsumer inputConsumer;
  protected final Runnable timeoutCallback;
  
  protected final Thread heartbeatSender = new Thread(() -> heartbeatLoop());

  public InputPeer(
      EncrypterService encrypter,
      DecrypterService decrypter,
      byte[] hash,
      DatagramSocket socket,
      Runnable timeoutCallback) 
      throws SocketException {
    this(
        encrypter,
        decrypter,
        hash,
        socket,
        new HeartbeatSink(),
        timeoutCallback
    );
  }
  
  public InputPeer(
      EncrypterService encrypter,
      DecrypterService decrypter,
      byte[] hash,
      DatagramSocket socket,
      InputConsumer inputConsumer,
      Runnable timeoutCallback) 
      throws SocketException {
    this.encrypter = encrypter;
    this.decrypter = decrypter;
    this.hash = hash;
    this.inputConsumer = inputConsumer;
    this.timeoutCallback = timeoutCallback;
    
    this.packet_send.setAddress(socket.getInetAddress());
    this.packet_send.setPort(socket.getPort());
    
    this.datagramSocket = socket;
    this.datagramSocket.setReceiveBufferSize(TOTAL_SOCKET_BACKLOG_BUFFER);
    //for heartbeat purposes
    this.datagramSocket.setSoTimeout(InputProtocol.TIMEOUT_MS);
    
    this.setPriority(MAX_PRIORITY);
    
    this.start();
  }

  public void shutdown() {
    LOGGER.info("Shutting down local port receiver " + this.datagramSocket.getLocalPort());
    try {
      sendShutdownPacket();
    } catch (IOException e) {
      e.printStackTrace();
    }
    this.datagramSocket.close();
    this.interrupt();
    this.heartbeatSender.interrupt();
    this.timeoutCallback.run();
  }
  
  public boolean isShutdown() {
    return this.datagramSocket.isClosed();
  }
  
  protected void handlePacket(byte[] payload)
      throws PacketHandlingException, ShutdownException {
    try {
      Pair<byte[], ParsedInput> hash_input = 
          InputProtocol.decryptPacket(this.decrypter, payload);
      
      if(!indenticalByteArrays(hash, hash_input.a)) {
        throw new SecurityException("Hash mismatch, getting spammed or network error");
      }
      
      this.inputConsumer.serviceInput(hash_input.b);
    } catch (ShutdownException e) {
      throw e;
    } catch (Exception e) {
      e.printStackTrace();
      throw new PacketHandlingException(e.getMessage());      
    }
  }
  
  //Arrays.compare(byte[], byte[]) throws an error on Android
  protected boolean indenticalByteArrays(byte[] a, byte[] b) {
    if(a.length != b.length) {
      return false;
    }
    for(int it = 0; it < a.length; ++it) {
      if(a[it] != b[it]) {
        return false;
      }
    }
    return true;
  }
  
  protected void heartbeatLoop() {
    this.setPriority(MAX_PRIORITY);
    ParsedInput heartbeatInput = new ParsedInput(InputType.HEARTBEAT);
    while(!datagramSocket.isClosed() && !interrupted()) {
      try {
        do {
          LOGGER.info("Sending heartbeat");
          synchronized(this) {
            sendPacket(heartbeatInput);
          }
          
          Thread.sleep(InputProtocol.HEARTBEAT_INTERVAL_MS);
        }while(true);
      } catch (IOException | InterruptedException e) {
        LOGGER.severe("Socket error");
        e.printStackTrace();
        this.shutdown();
      }
    }
  }
  
  public boolean sendShutdownPacket() throws IOException {
    ParsedInput input = new ParsedInput(InputType.SHUTDOWN);
    return sendPacket(input);
  }
  
  public synchronized boolean sendPacket(ParsedInput input) throws IOException {
    InputProtocol.preparePayload(
        encrypter,
        payload_send,
        hash,
        input);

    datagramSocket.send(packet_send);
    
    return true;
    
  }
  
  @Override
  public void start() {
    if(datagramSocket.isClosed()) {
      throw new IllegalStateException("Socket was closed already");
    }
    
    super.start();
    this.heartbeatSender.start();
  }
  
  @Override
  public void run() {
    while(!datagramSocket.isClosed() && !interrupted()) {
      try {
        do {
          datagramSocket.receive(packet_receive);
          handlePacket(this.payload_receive);
        }while(true);
      } catch (SocketTimeoutException te) {
        LOGGER.warning("Socket timed-out");
        this.shutdown();
      } catch (IOException e) {
        LOGGER.severe("Socket error");
        e.printStackTrace();
        this.shutdown();
      } catch (PacketHandlingException e) {
      } catch (ShutdownException e) {
        LOGGER.info("Shutdown of inputPeer");
        this.shutdown();
      }
    }
  }
}
