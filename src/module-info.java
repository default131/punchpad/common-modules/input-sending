module pl.mk.touchpad.inputSending {
  requires java.desktop;
  requires transitive pl.mk.touchpad.crypto;
  requires java.logging;
  exports pl.mk.touchpad.input;
}